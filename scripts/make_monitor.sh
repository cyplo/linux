#!/bin/sh
airmon-ng stop mon0 > /dev/null
airmon-ng stop mon1 > /dev/null
airmon-ng stop $1 > /dev/null
sh change_mac.sh $1 > /dev/null
airmon-ng start $1 > /dev/null
ifconfig -a | grep HWaddr

