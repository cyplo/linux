downlaoded from: http://www.realtek.com/downloads/downloadsView.aspx?Langid=1&PNid=15&PFid=25&Level=4&Conn=3&DownTypeID=3&GetDown=false#2


General Information
===================

Linux driver for Realtek PCI-Express card reader chip.


Build Steps
===========

1) make
2) make install
3) depmod
4) reboot your computer

Note: Root privilege is required in step 2 and 3
